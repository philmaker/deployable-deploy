
const kelda = require('kelda');
const {Container, Image} = require('kelda');

Deployment = function(options) {
	
	this.initialize(options);
};

Deployment.prototype = {
	
	initialize : function(options) {
		
		Object.assign(this, options);
		this.port = this.port || 80;
		this.scale = this.scale || 1;
		this.environment = {
			PORT: this.port.toString()
		};
		var name = this.name(this.repository);
		var dockerfile = this.dockerfile(this.repository);
		const image = new Image(name, dockerfile);
		this.containers = [];
		for (let i = 0; i < this.scale; i++) {
			this.containers.push(new Container('node-app', image, {
				command: this.command()
			}).withEnv(this.environment));
		}
		kelda.allow(kelda.publicInternet, this.containers, this.port);
		const infrastructure = kelda.baseInfrastructure();
		this.containers.forEach(function(container) {
			container.deploy(infrastructure);
		});
	},
	
	name : function(repository) {
		
		let result = [];
		result.push('node-app');
		const index = repository.lastIndexOf('/');
		if (index !== -1) {
			result.push(':');
			result.push(repository.slice(index + 1));
		}
		return result.join('');
	},
	
	dockerfile : function(repository) {
		
		var array = [];
		array.push('FROM node:6.8');
		array.push('RUN mkdir -p /usr/src/app');
		array.push('WORKDIR /usr/src/app');
		array.push('RUN git clone ' + repository + ' .');
		array.push('RUN npm install');
		return array.join('\r\n');
	},
	
	command : function() {
		
		if (true) {
			return ['npm', 'start'];
		} else {
			return ['tail', '-f', '/dev/null'];
		}
	}
};

const deployment = new Deployment({
	repository: 'https://bitbucket.org/philmaker/deployable',
	port : 80,
	scale: 1
});
